import sys
from math import ceil
from numpy import zeros

def edit_distance(str1, str2):
    # inizializzazione matrice di dimensione i+i, j+1
    m = zeros((len(str1)+1, len(str2)+1))

    # prima colonna = indice della prima stringa
    for i in range(0, len(str1)+1):
        m[i][0] = i
    
    # prima riga = indice della seconda stringa
    for i in range(0, len(str2)+1):
        m[0][i] = i

    # inserimento valori di distanze di edit di stringhe
    # prefisso
    for i in range(1, len(str1)+1):
        for j in range(1, len(str2)+1):
            # controllo: se i caratteri nella stessa posizione sono uguali
            if str1[i-1] == str2[j-1]:
                # la distanza di edit e` la distanza di edit dei prefissi i-1, j-1
                m[i][j] = m[i-1][j-1]
            else:
                # altrimenti la distanza di edit e` il minimo tra i seguenti valori:
                # [i-1, j]+1  : e` stata effettuata un'eliminazione
                # [i, j-1]+1  : e` stato effettuato un inserimento
                # [i-1, j-1]+1: e` stata effettuata una sostituzione

                m[i][j] = min(m[i-1][j]+1, m[i][j-1]+1, m[i-1][j-1]+1)

    return int(m[len(str1)][len(str2)])

def distance(x, y):
    # generazione dei token
    whitespace_split_1 = x.split(' ')
    whitespace_split_2 = y.split(' ')
    
    # intersezione
    intersect = 0;
    l1 = len(whitespace_split_1)
    l2 = len(whitespace_split_2)
    
    
    for i in range(l1):
        for j in range(l2):
            # confronta due stringhe
            # viene considerata la lunghezza
            # della stringa piu` grande e viene divisa per 4
            lunghezza = ceil(max(len(whitespace_split_1[i]), len(whitespace_split_2[j]))/4)
            
            # l'intersezione viene incrementata se
            # le due stringhe sono considerate simili per almeno 
            # un quarto (distanza di edit <= lunghezza)
            if edit_distance(whitespace_split_1[i],whitespace_split_2[j]) <= lunghezza:
                intersect += 1
                break
    div = intersect/float(l1+l2-intersect)

    # arrotondamento
    return round(1 - div, 1)

def complete_distance(str1, str2, str3, str4):
    d1 = distance(str1, str2)
    d2 = distance(str3, str4)
    if d1 > d2:
        print(str1, ',', str2, "simili con distanza", d1, "anziche'", str1, 'e', str2, "con distanza", d1)
    elif d1 < d2:    
    	print(str1, ',', str2, "simili con distanza", d1, "anziche'", str3, 'e', str4, "con distanza", d2)
    else:        
        print("Le stringhe sono simili in modo perfetto "+str(d1))

def main(s1, s2, s3, s4):
    complete_distance(s1, s2, s3, s4)

if __name__ == '__main__':
    try:
        main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    except IndexError:
        main()
    except (KeyboardInterrupt, EOFError):
        pass


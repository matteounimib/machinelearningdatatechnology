import csv
import sys
from uuid import uuid4
import os
import json
import multiprocessing

from pymongo import MongoClient
from tqdm import tqdm
import psutil

from func import min_max_attribute, attributeCitta, categorie, calcolo_punteggio

class Data:
    def __init__(self, citta, tipo):
        self.d, self.p = extendedAttribute2(citta)
        self.d["_id"] = citta
        self.p["_id"] = citta
        self.tipo = tipo
    def get(self, categoria):
        return self.d['categoria']

    def save(self):
        db = MongoClient('localhost', 27017)['lifequality']
        for i in self.d.keys():
            pass
        if self.d["_id"] == "Tipo":
            return
        db["valori"].insert_one(self.d)
        db["punteggi"].insert_one(self.p)

def readcsv(csv_file):
    d = []
    with open(csv_file, encoding="utf-8") as f:
        csv_reader = csv.reader(f, delimiter=",")
        header = next(csv_reader)
        for i in csv_reader:
            d.append(i)
    return (header, d)

def extendedAttribute2(citta):
    d = {}
    p = {}
    #os.chdir("../pureData")
    for i in categorie.keys():
        d[i] = dict() # categorie....
        p[i] = dict()
        for j in categorie[i][0]:
            d[i][j] = 0
            p[i][j] = 0
            
        for j in d[i].keys():
            d[i][j] = attributeCitta(citta, i, j)
            if not isinstance(d[i][j], str):
                p[i][j] = calcolo_punteggio(citta, i, j)
        #for i in p[i].keys():
            #if i[0] != "Tipo":
                #p[i][j] = calcolo_punteggio(citta, i, j)
    #os.chdir("../mongo")
    #print(d)
    return (d, p)

def mp_data(val):
    Data(val, "").save()

def insertZona(c):
    d = {}
    header, csv = readcsv(c)
    target = []
    for i in csv:
        tmpd={}
        citta = i[0]
        zona = i[2]
        tmpd["_id"]=citta
        tmpd["zona"]=zona
        target.append(tmpd)
    db = MongoClient('localhost', 27017)['lifequality']
    for i in target:
        db["valori"].update_one({'_id':i["_id"]},{'$set':{'zona':i["zona"]}})
#    db["valori"].update_one({'_id':'Agrigento'},{'$set':{'zona':'sud'}})

def main(multi, f):
    mp_enable = bool(int(multi))
    header, csv_list = readcsv(f)
    if mp_enable:
        multiqueue = []
        for i in csv_list:
            multiqueue.append(i[0])
        with multiprocessing.Pool(psutil.cpu_count()) as p:
            p.map(mp_data, multiqueue)
    else:
        for i in tqdm(csv_list):
             Data(i[0],"").save()

if __name__ == "__main__":
    try:
        main(sys.argv[1], sys.argv[2])
        insertZona('../pureData/pronvince_regioni.csv')
    except (KeyboardInterrupt, EOFError): 
        pass


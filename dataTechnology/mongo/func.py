import csv
import os

ROUND_CIFRE_PER_RIDURRE_IL_NUMERO_DI_NUMERI_DOPO_I_NUMERI_PRIMA_DELLA_VIRGOLA = 3

"""
Contro-ottimizzata, ma consistente
"""
def exctractCategorie(csv_file):
    os.chdir('../pureData')
    with open(csv_file, encoding="utf-8") as f:
        csv_reader = csv.reader(f, delimiter=",")
        header = next(csv_reader)[1:]
        tipi = next(csv_reader)
    os.chdir('../mongo')
    return (header, tipi)

categorie = dict(
    Giustizia = exctractCategorie("Valori_Giustizia.csv"),
    Affari = exctractCategorie("Valori_Affari.csv"),
    Cultura = exctractCategorie("Valori_Cultura.csv"),
    Demografia = exctractCategorie("Valori_Demografia.csv"),
    Reddito = exctractCategorie("Valori_Reddito.csv"),
    Servizi = exctractCategorie("Valori_Servizi.csv"),
)

def attributeCitta(citta, categoria, sub):
    os.chdir('../pureData')
    with open("Valori_"+categoria+".csv", encoding="utf-8") as f:
        csv_reader = csv.reader(f, delimiter=",")
        header = next(csv_reader)
        for i in csv_reader:
            if i[0] == citta:
                os.chdir('../mongo')
                try:
                    return float(i[categorie[categoria][0].index(sub)+1])
                except ValueError:
                    return i[categorie[categoria][0].index(sub)+1]


def increase_min(a):
    new_min = 0
    if min(a) == 0:
        for i in sorted(a):
            if i != 0:
                new_min = i
                return [i+new_min for i in a]
    else:
        return a

def min_max_attribute(categoria, sub):
    os.chdir('../pureData')
    with open("Valori_"+categoria+".csv", encoding="utf-8") as f:
        csv_reader = csv.reader(f, delimiter=",")
        header = next(csv_reader)
        tipi = next(csv_reader)
        tipo = tipi[header.index(sub)]
        column = []
        for i in csv_reader:
            val = float(i[header.index(sub)])
            column.append(val)
    
    os.chdir('../mongo')
    
    return (tipo, min(increase_min(column)), max(column))


def calcolo_punteggio_P(citta, categoria, sub):
    val = attributeCitta(citta, categoria, sub)
    
    return float(val/min_max_attribute(categoria, sub)[2] * 1000)


def calcolo_punteggio_N(citta, categoria, sub):
    
    val = attributeCitta(citta, categoria, sub)
    if val == 0:
        val = min_max_attribute(categoria, sub)[1]
    if sub == 'GapRetributivoUominiDonne' or sub == '%GapRetributivoItaStran':
        return float(((1/abs(val))/(1/abs(min_max_attribute(categoria, sub)[2])))*1000)

    return float(((1/val)/(1/min_max_attribute(categoria, sub)[1]))*1000)


def calcolo_punteggio_S(citta, categoria, sub):
    min_max = min_max_attribute(categoria, sub)
    x = 250 * (abs(min_max[1]) + min_max[2]) / 750
    
    return float(((attributeCitta(citta, categoria, sub) + abs(min_max[1]) + x) / (min_max[2] + abs(min_max[1]) + x)) * 1000)


tipo_funzione = dict(
    N=calcolo_punteggio_N,
    P=calcolo_punteggio_P,
    S=calcolo_punteggio_S,
)

def calcolo_punteggio(citta, categoria, sub):
    #print(citta, categoria, sub)
    #print(min_max_attribute(categoria, sub))
    return round(float(tipo_funzione[min_max_attribute(categoria, sub)[0]](citta, categoria, sub)), ROUND_CIFRE_PER_RIDURRE_IL_NUMERO_DI_NUMERI_DOPO_I_NUMERI_PRIMA_DELLA_VIRGOLA)

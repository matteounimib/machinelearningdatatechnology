<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Distributed Relational Database Architecture" FOLDED="false" ID="ID_866469586" CREATED="1546957885364" MODIFIED="1546960779471" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="8" RULE="ON_BRANCH_CREATION"/>
<node TEXT="basics" POSITION="right" ID="ID_1105345706" CREATED="1546960802504" MODIFIED="1546960803867">
<edge COLOR="#ff0000"/>
<node TEXT="Abilita comunicazione tra&#xa;application system&#xa;e database system" ID="ID_303985569" CREATED="1546960805890" MODIFIED="1546960837555"/>
<node TEXT="Diverse piattaforme" ID="ID_1464559797" CREATED="1546960840968" MODIFIED="1546960872885"/>
<node TEXT="Diversi vendor" ID="ID_1112771668" CREATED="1546960873037" MODIFIED="1546960876949"/>
</node>
<node TEXT="Distribuited Data Manager" POSITION="left" ID="ID_852822203" CREATED="1546960965191" MODIFIED="1546960980751">
<edge COLOR="#0000ff"/>
<node TEXT="Struttura di comandi e risposte per&#xa;db distribuiti" ID="ID_1265269084" CREATED="1546960986253" MODIFIED="1546961104367"/>
<node TEXT="Meno di 20 comandi necessari&#xa;per implementare comunicazione&#xa;tra server e client" ID="ID_1992157057" CREATED="1546961107520" MODIFIED="1546961154462"/>
</node>
<node TEXT="DRDA Reference" POSITION="left" ID="ID_1170180719" CREATED="1546961175956" MODIFIED="1546961251777">
<edge COLOR="#ff00ff"/>
<node TEXT="Descrive la connessione necessaria tra" ID="ID_337546806" CREATED="1546961260008" MODIFIED="1546961273529">
<node TEXT="applicazione" ID="ID_1845352294" CREATED="1546961278489" MODIFIED="1546961282769"/>
<node TEXT="dbms in un contesto distribuito" ID="ID_1953489708" CREATED="1546961282971" MODIFIED="1546961300687"/>
</node>
<node TEXT="Descrive responsabilita` dei partecipanti" ID="ID_1310600500" CREATED="1546961372826" MODIFIED="1546961395286"/>
<node TEXT="Descrive formati e protocolli" ID="ID_1073643856" CREATED="1546961396748" MODIFIED="1546961410692"/>
</node>
<node TEXT="Unit of  Work (o transaction)" POSITION="right" ID="ID_1891205142" CREATED="1546961420420" MODIFIED="1546961445501">
<edge COLOR="#00ffff"/>
<node TEXT="si riferisce ad una sequenza di&#xa;operazioni che un db deve processare" ID="ID_665300534" CREATED="1546961447556" MODIFIED="1546961624599"/>
<node TEXT="Generalmente termina con una commit" ID="ID_95252793" CREATED="1546961614710" MODIFIED="1546961637365"/>
<node TEXT="Devono rispettare le ACID" ID="ID_332005644" CREATED="1546961637746" MODIFIED="1546961646799"/>
<node TEXT="In sql sono espresse da select (mah?)" ID="ID_555069768" CREATED="1546961647143" MODIFIED="1546961680474"/>
</node>
<node TEXT="DML SQL language" POSITION="right" ID="ID_1330458280" CREATED="1546961729876" MODIFIED="1546961737386">
<edge COLOR="#7c0000"/>
<node TEXT="Estensione SQL per drda" ID="ID_2424454" CREATED="1546961745240" MODIFIED="1546961753508"/>
<node TEXT="in automatico inoltra le richieste ai db remoti&#xa;e li` la richiesta viene processata" ID="ID_390823574" CREATED="1546961753747" MODIFIED="1546961881443"/>
<node TEXT="Prima di eseguire la richiesta il drda deve&#xa;effettuare una connessione con il db remoto&#xa;dove risiede l&apos;informazione" ID="ID_683169717" CREATED="1546961896932" MODIFIED="1546961918468">
<node TEXT="SQL CONNECT statement&#xa;fornito dal drda" ID="ID_1490240275" CREATED="1546961984024" MODIFIED="1546962032654"/>
<node TEXT="SELECT CONNECT to DB1" ID="ID_80341168" CREATED="1546962057116" MODIFIED="1546962066048"/>
</node>
</node>
<node TEXT="Livelli di distribuzione" POSITION="left" ID="ID_693027074" CREATED="1546962084685" MODIFIED="1546962089610">
<edge COLOR="#00007c"/>
<node TEXT="0. richiesta remota" ID="ID_785914430" CREATED="1546962091858" MODIFIED="1546962097079">
<node TEXT="una sola operazione" ID="ID_1488649803" CREATED="1546964101966" MODIFIED="1546964107797"/>
</node>
<node TEXT="1. unit of work remota" ID="ID_666897688" CREATED="1546962097342" MODIFIED="1546962105179">
<node TEXT="Piu` operazioni&#xa;(select ed insert)" ID="ID_103834326" CREATED="1546964109915" MODIFIED="1546964127729"/>
</node>
<node TEXT="2. unit of work distribuita" ID="ID_1648309305" CREATED="1546962105353" MODIFIED="1546962113463">
<node TEXT="Due fasi di lock per isolation" ID="ID_112529773" CREATED="1546964039408" MODIFIED="1546964053350"/>
<node TEXT="Commit distribuita in due fasi per&#xa;Atomicita` e Durabilita`" ID="ID_198697577" CREATED="1546964053569" MODIFIED="1546964085617">
<node TEXT="Presente m eccanismo di rollback&#xa;nel caso una delle due commit non andasse&#xa;a buon fine" ID="ID_252280351" CREATED="1546964580895" MODIFIED="1546964600015"/>
<node TEXT="Ruoli" ID="ID_1440186261" CREATED="1546964819142" MODIFIED="1546964820778">
<node TEXT="Coordinator" ID="ID_850565692" CREATED="1546964821922" MODIFIED="1546964824220"/>
<node TEXT="Partecipant" ID="ID_800633522" CREATED="1546964825101" MODIFIED="1546964827215"/>
</node>
<node TEXT="fasi" ID="ID_1886903269" CREATED="1546964671388" MODIFIED="1546964715899">
<node TEXT="Local SQL" ID="ID_1241422400" CREATED="1546964717227" MODIFIED="1546964722555"/>
<node TEXT="Send remote sql1" ID="ID_548649797" CREATED="1546964723451" MODIFIED="1546964726943"/>
<node TEXT="send remote sql2" ID="ID_991227594" CREATED="1546964728441" MODIFIED="1546964732690"/>
<node TEXT="preparazione commit" ID="ID_1847862985" CREATED="1546964733250" MODIFIED="1546964750234"/>
<node TEXT="Risposta partecipante (ok/no)" ID="ID_1174037315" CREATED="1546964750447" MODIFIED="1546964759233"/>
<node TEXT="commit o abort&#xa;(partecipante: commit o rollback)" ID="ID_959711331" CREATED="1546964759592" MODIFIED="1546964788027"/>
<node TEXT="complete" ID="ID_52746754" CREATED="1546964788666" MODIFIED="1546964791065"/>
</node>
</node>
<node TEXT="PIu` database" ID="ID_935081827" CREATED="1546964136908" MODIFIED="1546964142418"/>
</node>
<node TEXT="3. richiesta distribuita" ID="ID_36735130" CREATED="1546962116648" MODIFIED="1546962124662">
<node TEXT="Richieste a multipli (o molteplici?) DBMS" ID="ID_833039840" CREATED="1546964875094" MODIFIED="1546964901520"/>
<node TEXT="Esempio: join tra due tabelle estratte da&#xa;due dbms diversi" ID="ID_1830314546" CREATED="1546964914708" MODIFIED="1546964941842"/>
<node TEXT="l&apos;applicazione fa la richiesta al dbms" ID="ID_153210280" CREATED="1546965055606" MODIFIED="1546965061870"/>
<node TEXT="il dbms distribuisce la richiesta a vari dbms" ID="ID_843327235" CREATED="1546965062033" MODIFIED="1546965095465"/>
<node TEXT="Trasparenza" ID="ID_699079186" CREATED="1546965177786" MODIFIED="1546965180441">
<node TEXT="Location" ID="ID_1175035926" CREATED="1546965182182" MODIFIED="1546965187169">
<node TEXT="Se l&apos;informazione viene spostata&#xa;i riferimenti (nicknames) possono essere&#xa;aggiornati senza alcun cambiamento all&apos;applicazione" ID="ID_864112387" CREATED="1546965494640" MODIFIED="1546965560338"/>
</node>
<node TEXT="Logical" ID="ID_116685038" CREATED="1546965188024" MODIFIED="1546965202173"/>
<node TEXT="Physical" ID="ID_861665299" CREATED="1546965202578" MODIFIED="1546965210037"/>
</node>
<node TEXT="Compensation" ID="ID_1123060417" CREATED="1546965574318" MODIFIED="1546966276247"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ottimizzazioni per i dbms che
    </p>
    <p>
      non supportano tutto il DB2 SQL dialect (IBM).
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Schemi" ID="ID_1157432027" CREATED="1546966252369" MODIFIED="1546966289968">
<node TEXT="Local schema" ID="ID_411935047" CREATED="1546966291756" MODIFIED="1546966330136"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      schema di ogni nodo
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Global Schema" ID="ID_1058962085" CREATED="1546966296600" MODIFIED="1546966354494"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      schema globale comune per tutti i nodi
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Integrazione fattibile ad esempio con views" ID="ID_1646289579" CREATED="1546966300122" MODIFIED="1546966311074"/>
</node>
<node TEXT="Frammentazione" ID="ID_671423288" CREATED="1546966388554" MODIFIED="1546966487420"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Situazione in cui i dati si trovano
    </p>
    <p>
      su dbms diversi
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Orizzontale" ID="ID_885107428" CREATED="1546966490211" MODIFIED="1546966553988"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Esempio: db di studenti di Lombardia
    </p>
    <p>
      e db con studenti di Piemonte
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Verticale" ID="ID_1906559159" CREATED="1546966493888" MODIFIED="1546966586321"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Esempio: db con cognome studente
    </p>
    <p>
      db con regione di provenienza dello studente
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="AR  e AS" ID="ID_575870850" CREATED="1546963102795" MODIFIED="1546963117588">
<node TEXT="Application Request:&#xa;applicazione che invia richiesta sul network" ID="ID_1562105173" CREATED="1546963118147" MODIFIED="1546963266347"/>
<node TEXT="Application Server:&#xa;Sistema remoto che esegue richieste SQL" ID="ID_1042524693" CREATED="1546963267097" MODIFIED="1546963303639"/>
<node TEXT="Ci sono piattaforme i cui componenti&#xa;hanno sia il ruolo di AR sia quello di AS" ID="ID_606135187" CREATED="1546963309367" MODIFIED="1546963334505"/>
</node>
<node TEXT="" ID="ID_666406873" CREATED="1546962197616" MODIFIED="1546962793702">
<hook URI="tab.png" SIZE="0.9118541" NAME="ExternalObject"/>
</node>
</node>
</node>
</map>

<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Introduzione DBMS non centralizzati" FOLDED="false" ID="ID_1742941584" CREATED="1546958907349" MODIFIED="1546961871760" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="14" RULE="ON_BRANCH_CREATION"/>
<node TEXT="" POSITION="right" ID="ID_1609629727" CREATED="1546959107110" MODIFIED="1546959770465">
<edge COLOR="#7c007c"/>
<node TEXT="" ID="ID_869089556" CREATED="1546959032779" MODIFIED="1546959107113">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Proprieta&apos; ACID" ID="ID_1554388370" CREATED="1546959003463" MODIFIED="1546959107123"/>
<node TEXT="" ID="ID_835910957" CREATED="1546959032776" MODIFIED="1546959201106">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Atomicit&#xe0;" ID="ID_562171205" CREATED="1546959032781" MODIFIED="1546960689222" HGAP_QUANTITY="58.99999865889553 pt" VSHIFT_QUANTITY="-27.74999917298558 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ogni transazione deve essere eseguita o tutta o per niente, non pu&#242; essere eseguita solo in parte.
    </p>
  </body>
</html>

</richcontent>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Atomicit&#224; e durabilit&#224; sono fortemente legati: le politiche di ripristino devono essere compatibili con le politiche di salvataggio e della atomicit&#224; delle transazioni
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Consistenza" ID="ID_895823708" CREATED="1546959140726" MODIFIED="1546959621879" HGAP_QUANTITY="60.49999861419205 pt" VSHIFT_QUANTITY="-6.749999798834331 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Le transizione devono lasciare il DB in uno stato consistente o valido. Indipendetemente da come sia terminata la transazione i dati modificati dalla transazione stessa devono rispettare tutti i vincoli di integrit&#224;.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Isolamento" ID="ID_449351705" CREATED="1546959189503" MODIFIED="1546960107339" HGAP_QUANTITY="67.99999839067465 pt" VSHIFT_QUANTITY="-17.249999485909953 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Pur in un contesto di concorrenza il risultato di un insieme di transazioni, T1, T2, &#232; lo stesso di una qualunque esecuzione sequenziale di T1, T2.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Durabilita&apos;" ID="ID_1077677610" CREATED="1546959198535" MODIFIED="1546960536256" HGAP_QUANTITY="66.49999843537813 pt" VSHIFT_QUANTITY="-8.999999731779106 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Se una transazione viene eseguita con successo, gli update vengono salvati anche se il database crasha subito dopo aver eseguito il commit. I log delle transazioni vengono salvati in modo che il database venga ripristinato nello stato subito precedente del guasto.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="" POSITION="left" ID="ID_742612355" CREATED="1546961864325" MODIFIED="1546961864326">
<edge COLOR="#0000ff"/>
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Tipi non centralizzati" POSITION="left" ID="ID_1525779107" CREATED="1546961839496" MODIFIED="1546965107410" HGAP_QUANTITY="46.99999901652339 pt" VSHIFT_QUANTITY="-0.7499999776482589 pt">
<edge COLOR="#7c7c00"/>
</node>
<node TEXT="" POSITION="left" ID="ID_1942766323" CREATED="1546961864324" MODIFIED="1546966081309">
<edge COLOR="#ff0000"/>
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Distribuited Access" ID="ID_67067587" CREATED="1546961864327" MODIFIED="1546962281466" HGAP_QUANTITY="46.24999903887513 pt" VSHIFT_QUANTITY="-9.749999709427366 pt" TEXT_SHORTENED="true">
<hook URI="../../Desktop/Data%20attack/Distribuited%20access.bmp" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      I DBMS sono stessi modelli
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Replication" ID="ID_1150051272" CREATED="1546962112464" MODIFIED="1546965110116" HGAP_QUANTITY="49.99999892711643 pt" VSHIFT_QUANTITY="9.749999709427371 pt" TEXT_SHORTENED="true">
<hook URI="../../Desktop/Data%20attack/replication.bmp" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Stessi modelli o spesso&#160;&#160;sono modelli eterogenei (colori differenti)
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Replication service" ID="ID_155831596" CREATED="1546962423541" MODIFIED="1546962665841" HGAP_QUANTITY="75.49999816715723 pt" VSHIFT_QUANTITY="68.99999794363981 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      I dati vengono fisicamente copiati da un sistema ad un altro in maniera modo direzionale o bidirezionale
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Topologie di copiatura" ID="ID_253531567" CREATED="1546962564335" MODIFIED="1546962774879" HGAP_QUANTITY="70.24999832361942 pt" VSHIFT_QUANTITY="-66.74999801069504 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ci sono diverse varianti tra cui:
    </p>
    <p>
      One-to-One
    </p>
    <p>
      One-to-Many
    </p>
    <p>
      Many-to-One
    </p>
    <p>
      Fan-Out
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Federation" ID="ID_334856590" CREATED="1546962783607" MODIFIED="1546966042780" HGAP_QUANTITY="72.4999982565642 pt" VSHIFT_QUANTITY="18.74999944120647 pt" TEXT_SHORTENED="true">
<hook URI="../../Desktop/Data%20attack/federation.bmp" SIZE="1.0" NAME="ExternalObject"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Solitamente sono modelli eterogenei (Colori differenti)
    </p>
  </body>
</html>

</richcontent>
<node TEXT="E&apos; un&apos;estensione del modello Distribuited Access per ambienti eterogenei" ID="ID_1209677033" CREATED="1546964660167" MODIFIED="1546964767134" HGAP_QUANTITY="71.74999827891594 pt" VSHIFT_QUANTITY="-20.99999937415126 pt"/>
<node TEXT="L&apos;applicazione si connette ad un singolo database virtuale" ID="ID_1259570445" CREATED="1546964718168" MODIFIED="1546964935024" HGAP_QUANTITY="99.4999974519015 pt" VSHIFT_QUANTITY="-41.99999874830249 pt"/>
</node>
<node TEXT="Data Warehouse" FOLDED="true" ID="ID_243575722" CREATED="1546965114016" MODIFIED="1546965516725" HGAP_QUANTITY="78.49999807775026 pt" VSHIFT_QUANTITY="9.749999709427366 pt">
<hook URI="../../Desktop/Data%20attack/Datawarehouse.bmp" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Il modello ETL (Extract - Load - Transform) &#xe8; una estensione del modello di Replication" ID="ID_1242077751" CREATED="1546965429735" MODIFIED="1546965515104" HGAP_QUANTITY="96.49999754130847 pt" VSHIFT_QUANTITY="-47.99999856948857 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Possono essere usate altre varianti come: ELT, TEL
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="L&apos;ETL &#xe8; il sistema chiave per inserire i dati all&apos;interno della DW" ID="ID_1726868433" CREATED="1546965510608" MODIFIED="1546965689419" HGAP_QUANTITY="103.24999734014281 pt" VSHIFT_QUANTITY="-71.24999787658459 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      I dati vengono estratti da delle risorse (solitamente DB)
    </p>
    <p>
      I dati vengono trasformati in un formato accettabile per l'ambiete della DW
    </p>
    <p>
      I dati vengono caricati all'interno del DBMS della DW.
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Event publishing" ID="ID_1587443958" CREATED="1546966066312" MODIFIED="1546966081308" HGAP_QUANTITY="76.24999814480549 pt" VSHIFT_QUANTITY="15.749999530613437 pt"/>
</node>
</node>
</map>

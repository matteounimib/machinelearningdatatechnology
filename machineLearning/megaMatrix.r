
###ONLY CLUSTER#####
megaMatrix = matrix(ncol=9,nrow=6)
colnames(megaMatrix) = c("true_positive","true_negative","false_positive","false_negative","accuracy","precision","recall","f_measure","specifity")
rownames(megaMatrix) = c("reddito","giustizia","demografia","cultura","servizi","affari")
megaMatrix = as.data.frame(megaMatrix)

valueMatrix = function(categoria,i)
{
  return(table(zonaArray,categoria)[[i]])
}

confusion_Matrix = function(nome,categoria){
  #categoria = frame.cluster$categoria
  return(table(zonaArray,categoria))
}

accuracy = function(matriceConfusione){
  return (sum(diag(matriceConfusione))/sum(matriceConfusione))
}

precision= function(matriceConfusione){
  return (matriceConfusione[[1]]/(matriceConfusione[[1]] + matriceConfusione[[2]]))
}

recall = function(matriceConfusione){
  return(matriceConfusione[[1]]/(matriceConfusione[[1]] + matriceConfusione[[3]]))
}

f_measure = function(precision,recall){
  return(1/(((1/recall)+(1/precision))/2))
}

specifity = function(matriceConfusione)
{
  return(matriceConfusione[[4]] / (matriceConfusione[[4]]+matriceConfusione[[2]]))    
}

fillMatri = function(categ)
{
  nome=deparse(substitute(categ))
  appoggio=categ
  
  megaMatrix$true_positive[match(nome,rownames(megaMatrix))]=valueMatrix(frame_cluster[[nome]],1)#frame_cluster$reddito,1    frame_cluster$nome)
  megaMatrix$false_positive[match(nome,rownames(megaMatrix))]=valueMatrix(frame_cluster[[nome]],2)#frame_cluster$reddito,2)
  megaMatrix$false_negative[match(nome,rownames(megaMatrix))]=valueMatrix(frame_cluster[[nome]],3)#frame_cluster$reddito,3)
  megaMatrix$true_negative[match(nome,rownames(megaMatrix))] =valueMatrix(frame_cluster[[nome]],4)#frame_cluster$reddito,4)
  
  megaMatrix$accuracy[match(nome,rownames(megaMatrix))] = accuracy(confusion_Matrix(appoggio,frame_cluster[[nome]]))#reddito,frame_cluster$reddito))
  megaMatrix$precision[match(nome,rownames(megaMatrix))] = precision(confusion_Matrix(appoggio,frame_cluster[[nome]]))#reddito,frame_cluster$reddito))
  megaMatrix$recall[match(nome,rownames(megaMatrix))] = recall(confusion_Matrix(appoggio,frame_cluster[[nome]]))#reddito,frame_cluster$reddito))
  megaMatrix$f_measure[match(nome,rownames(megaMatrix))] = f_measure(megaMatrix$precision[match(nome,rownames(megaMatrix))],megaMatrix$recall[match(nome,rownames(megaMatrix))])
  megaMatrix$specifity[match(nome,rownames(megaMatrix))] = specifity(confusion_Matrix(appoggio,frame_cluster[[nome]]))
  return(megaMatrix)
}

for(i in 1:6)
{
  for(j in 1:9)
    {
    megaMatrix[i,j]=fillMatri(reddito)[i,j]
    megaMatrix[i,j]=fillMatri(giustizia)[i,j]
    megaMatrix[i,j]=fillMatri(demografia)[i,j]
    megaMatrix[i,j]=fillMatri(cultura)[i,j]
    megaMatrix[i,j]=fillMatri(servizi)[i,j]
    megaMatrix[i,j]=fillMatri(affari)[i,j]
  }
}
View(megaMatrix)

